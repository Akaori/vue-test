import Vue from 'vue'
import Vuex from 'vuex'
import API from '@/api'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    articles: [],
    length: 0,
    isLoading: false,
    isButtonLoading: false,
  },
  mutations: {
    SET_ARTICLES (state, response) {
      state.articles = response
      state.length = state.length + response.length
      state.isLoading = false
    },
    LOAD_MORE_ARTICLES (state, response) {
      if (response.length > 1) {
        response.forEach(item => state.articles.push(item));
      } else {
      }
      state.isButtonLoading = false
    },
    ORDER_ARTICLES (state, descending) {
      state.articles.sort(function (a, b) {
        return (a.publishedAt < b.publishedAt) ? -1 : ((a.publishedAt > b.publishedAt) ? 1 : 0);
      })
      if (descending) {
        state.articles.reverse()
      }
      state.isLoading = false
    },
    FILTER_ARTICLES (state, response) {
      state.articles = response
      state.isLoading = false
    }
  },
  actions: {
    async fetchData ({ commit }) {
      await API.get('articles?_limit=10').then(response => {
        commit('SET_ARTICLES', response.data)
      })
    },
    loadMore ({ commit, state }) {
      setTimeout(() => {
        API
          .get(`articles?_limit=10&_start=${state.length}`)
          .then(response => {
            commit('LOAD_MORE_ARTICLES', response.data)
          })
          .catch(err => {
            console.log(err);
          });
      }, 500);
    },
    filterArticles ({ commit, state }, search) {
      let url = `articles?_limit=${state.length}`
      for (const idx in search) {
        url += `&title_contains=${search[idx]}`
      }
      API
        .get(url)
        .then(response => {
          commit('FILTER_ARTICLES', response.data)
        }).catch(err => {
          console.log(err);
        });
    }
  },
  modules: {
  }
})
