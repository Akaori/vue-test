import Vue from 'vue'
import VueRouter from 'vue-router'
import ArticlesList from '@/views/ArticlesList.vue'
import PageNotFound from '@/views/PageNotFound.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'ArticlesList',
    component: ArticlesList
  },
  {
    path: '/404',
    alias: '*',
    name: 'PageNotFound',
    component: PageNotFound
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
