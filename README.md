# Front-end Challenge 🏅 2021 - Space Flight News

>  This is a challenge by [Coodesh](https://coodesh.com/)

## Descrição

Aplicação web para consumir as informações da API [Space Flight News](https://api.spaceflightnewsapi.net/v3/documentation), uma API pública com informações relacionadas a voos espaciais. 

O projeto tem como objetivo exibir os dados dos artigos, com o título, imagem, resumo e data de publicação. 

## Detalhes Técnicos
- Linguagem: JavaScript
- Tecnologias: Vue.js, VueX, Vuetify, Axios, Docker

### Instruções de instalação

> Requisitos para rodar o projeto:
> - Ter o git instalado no terminal de comando
> - Ter o docker e docker-compose instalados

- Clonar o repositório

```
git clone https://github.com/Akaori/vue-test.git
``` 

- Mudar para o repositório gerado

```
cd vue-test
```

- Rodar o docker

> Esta etapa pode ser um pouco demorada, pois precisará construir a imagem do projeto.

```
docker-compose up
```

- Acessar a página

http://localhost:8080/
